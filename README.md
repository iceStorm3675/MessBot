# MessBot
Hi there, this is a project to quickly help you for building a basic **Facebook Messenger ChatBot**. Using **NodeJS**, I've been creating some classes and conventions to work more effectly.


### How to use ?
>- Clone with Git from terminal: **```git clone https://github.com/iceStorm/MessBot.git```**
>- Or directly download as zip: **[https://github.com/iceStorm/MessBot/archive/master.zip](https://github.com/iceStorm/messenger-chatbot-framework/archive/master.zip)**


### Supported to
  >- Create and send:
  >   - **[Text message](https://developers.facebook.com/docs/messenger-platform/reference/send-api)**
  >   - **[Media template](https://developers.facebook.com/docs/messenger-platform/send-messages/template/media)**
  >   - **[Button template](https://developers.facebook.com/docs/messenger-platform/send-messages/template/button)**
  >   - **[Generic template](https://developers.facebook.com/docs/messenger-platform/send-messages/template/generic)**
  >   - **[Quick reply buttons](https://developers.facebook.com/docs/messenger-platform/reference/buttons/quick-replies)**
  >- Establish:
  >   - **[Typing action](https://developers.facebook.com/docs/messenger-platform/send-messages/sender-actions)**
  >   - **[Persistent menu](https://developers.facebook.com/docs/messenger-platform/send-messages/persistent-menu)**
  >   - **[Get Started](https://developers.facebook.com/docs/messenger-platform/reference/messenger-profile-api/get-started-button)** event
  >   - **[Greeting](https://developers.facebook.com/docs/messenger-platform/reference/messenger-profile-api/greeting)** message
  >   - **Chat session** (the bot will wait user for replying a specified response, then use it to handle something)
  >- Other:
  >   - **[Retrieve facebook user's basic profile](https://developers.facebook.com/docs/messenger-platform/identity/user-profile)** (name, first_name, last_name, profile_pic)


# Documents
  ### Browse our Wiki: [https://github.com/iceStorm/MessBot/wiki/Preparing](https://github.com/iceStorm/MessBot/wiki/Preparing).
  ### Or documentation page: [https://icestorm.github.io/MessBot/](https://icestorm.github.io/MessBot/).
