document.addEventListener('DOMContentLoaded', function()
{
    doClone();
    enableHighlightCode();

});


function enableHighlightCode()
{
    var pres = document.querySelectorAll("pre>code");
    for (var i = 0; i < pres.length; i++)
    {
        hljs.highlightBlock(pres[i]);
    }
    
    // add HighlightJS-badge (options are optional)
    var options =
    {   // optional
        contentSelector: "#ArticleBody",
    
        // CSS class(es) used to render the copy icon.
        copyIconClass: "fas fa-copy",
        // CSS class(es) used to render the done icon.
        checkIconClass: "fas fa-check text-success"
    };

    window.highlightJsBadge(options);
}


function doClone()
{
    let cloneBtn = $('#clone');
    cloneBtn.click(()=>
    {
        let textArea = document.createElement('textarea');
        textArea.style.top = "0";
        textArea.style.left = "0";
        textArea.style.position = "fixed";
        textArea.value = 'git clone https://github.com/iceStorm/MessBot.git';
        document.body.appendChild(textArea);
        textArea.focus();
        textArea.select();


        toastr.options.closeMethod = 'fadeOut';
        toastr.options.closeDuration = 100;
        toastr.options.closeEasing = 'swing';


        try
        {
            let successful = document.execCommand("copy");

            if (successful)
                toastr.success('Copied the git command to your clipboard !', 'MessBot');
            else
                toastr.error('Copy failed !', 'MessBot');
        }
        catch (err)
        {
            toastr.error('Copy failed !', 'MessBot');
        }
        finally
        {
            document.body.removeChild(textArea);
        }
    });
}