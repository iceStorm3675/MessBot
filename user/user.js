
require('dotenv').config();
const request = require('request');
const mongoClient = require('mongodb').MongoClient;
const connectionString = process.env.MONGO_CONNECTION_STRING;



class User
{

    constructor(userID)
    {
        this.ID = userID;
        this.Session = null;
        this.FirstName = null;
        this.LastName = null;
        this.FullName = null;
        this.AvatarLink = null;


        return new Promise(async (resolve, reject) =>
        {
            try
            {
                mongoClient.connect(connectionString, {useUnifiedTopology: true}, (err, db) =>
                {
                    if (err) return reject(err);
    
    
                    let dbo = db.db("FacebookUsers");
                    dbo.collection('Profile').findOne({ID: `${this.ID}`}, async (err, result) =>   //  Find facebook user in the 'Profile' collection of mongoDB
                    {
                        if (err) return reject(err);
                        if (result === null) //  Don't file any users in the mongoDB ==> need to get new user profile from Facebook API
                        {

                            //  Get new user profile
                            this.getNewUserProfile()
                                .then(profile =>
                                {
                                    if (profile === undefined)
                                    {
                                        console.log('Can\'t get profile of:', this.ID);
                                    }
                                    else
                                    {
                                        //  Assingn new user profile
                                        this.Session = '';
                                        this.FirstName = profile.first_name;
                                        this.LastName = profile.last_name;
                                        this.FullName = profile.name;
                                        this.AvatarLink = profile.profile_pic;
                                        console.log(`Successfully getted user profile: ${this.ID} — [${this.FullName}]`);
            
            
                                        //  Saving the new user profile
                                        db.close();
                                        return this.saveNewUserProfile(dbo);
                                    }
                                })
                                    .then(() =>
                                    {
                                        // console.log('Before resolve..');
                                        return resolve(this);
                                    })
                                        .catch(err =>
                                        {
                                            console.log('user.js:72 - Error', err);
                                        });
                        }
                        else
                        {
                            this.Session = result.Session;
                            this.FirstName = result.FirstName;
                            this.LastName = result.LastName;
                            this.FullName = result.FullName;
                            this.AvatarLink = result.AvatarLink;
                            console.log('\nDetected old user:', this);
                            return resolve(this);
                        }
                    });
                });
            }
            catch (err)
            {
                console.log('Error:', err);
            } 
        });
    }



    getNewUserProfile()
    {
        console.log('\nGetting new user profile:', this.ID);
        return new Promise((resolve, reject) =>
        {
            request
            (
                {
                    method: 'GET',
                    url: `https://graph.facebook.com/${this.ID}`,
                    qs:
                    {
                        fields: 'name, first_name, last_name, profile_pic',
                        access_token: process.env.PAGE_ACCESS_TOKEN
                    },
                    headers:
                    {
                        'Accept': 'application/json',
                        'Accept-Charset': 'utf-8'
                    }
                },
                (err, res, body) =>
                {
                    if (err || (res.statusCode !== 200))
                    {
                        return reject(undefined);
                    }
    
    
                    let bodyJSONstring = JSON.stringify(JSON.parse(body));
                    return resolve(JSON.parse(bodyJSONstring));
                }
            );
        });
    }

    saveNewUserProfile()
    {
        console.log(`Saving new user profile: ${this.ID} — [${this.FullName}]`);
        return new Promise((resolve, reject) =>
        {
            mongoClient.connect(connectionString, (err, db) =>
            {
                if (err)
                {
                    db.close();
                    return reject(err);
                }

                let dbo = db.db("FacebookUsers");
                dbo.collection('Profile').insertOne(this, (err, result) =>
                {
                    if (err)
                    {
                        db.close();
                        return reject(err);
                    }

                    console.log(`Saved new user: ${this.ID} — [${this.FullName}]`);
                    db.close();
                    return resolve();
                });
            });
        });
    }

    changeSession(sessionTag)
    {
        mongoClient.connect(connectionString, (err, db) =>
        {
            if (err) throw err;


            let dbo = db.db("FacebookUsers");
            dbo.collection("Profile").updateOne({ID: `${this.ID}`}, {$set: {Session: `${sessionTag}`}}, (err, result) =>
            {
                if (err)
                {
                    db.close();
                    throw err;
                }

                console.log(`Changed session to: "${sessionTag}" for: ${this.ID} — [${this.FullName}]`);
                db.close();
            });
        });
    }



    static getAllUserIDs()
    {
        console.log('\nGetting all users id ...');
        return new Promise((resolve, reject) =>
        {
            mongoClient.connect(connectionString, (err, db) =>
            {
                if (err)
                {
                    return reject(err);
                }
                else
                {
                    let dbo = db.db("FacebookUsers");
                    dbo.collection("Profile").distinct("ID", (err, result) =>   // Get all ID field(s) of the Profile collection
                    {
                        if (err)
                        {
                            db.close();
                            return reject(err);
                        }
                        else
                        {
                            db.close();
                            console.log(`Getted: ${result.length} users id.`);
                            return resolve(result);
                        }
                    });
                }
            })
        });
    }
}


module.exports = User;