
require('dotenv').config();
const request = require('request');
const user = require('../user/user');


class Bot
{
    #accessToken = process.env.PAGE_ACCESS_TOKEN;       //  Private field - Can only be used in this class

    
    constructor(webhookRequest, cloner = null)  //  cloner: the initialized bot you want to pass from
    {
        this.hasMessage = false;
        this.receivedText = null;
        this.isQuickReply = false;
        this.isPostBack = false;
        this.payload = null;
        this.timestamp = null;
        this.recipientID = null;
        this.attachments = null;
        this.sender = null;


        return new Promise(async (resolve, reject) =>
        {
            if (cloner === null)    //  When you don't pass the cloner param, we need to initialize new bot
            {
                let messaging = webhookRequest.body.entry[0].messaging[0];
                console.log('\n\n——————————————————————————————————————————————————————————————————————————————————————————');
                console.log('Received messaging:', messaging);
                let senderID = messaging.sender.id;
                this.timestamp = messaging.timestamp;
                this.recipientID = messaging.recipient.id;
    
    
                //  If user sent a text massage
                if (messaging.message)
                {
                    this.hasMessage = true;
                    let message = messaging.message;
    

                    if (message.text)
                    {
                        this.receivedText = message.text;
                        if (message.quick_reply)            //  If contain the quickreply buttons
                        {
                            this.isQuickReply = true;
                            if (message.quick_reply.payload)    //  If the quickreply buttons have payload
                                this.payload = message.quick_reply.payload;
                        }
                    }
                    else if (message.attachments)
                    {
                        this.attachments = message.attachments;
                        console.log('\nDetail attachments: ');
                        for (let i = 0; i < this.attachments.length; ++i)
                        {
                            console.log(this.attachments[i]);
                        }
                    }
                }
                else if (messaging.postback)       //  Otherwise, the user clicked a postback component
                {
                    this.isPostBack = true;
                    this.payload = messaging.postback.payload;
                }
    
                
                //  Getting the informations of sender (facebook user) - this may need await cause connecting to the Mongo Cloud
                this.sender = await new user(senderID);
                return resolve(this);
            }
            else
            {
                Object.assign(this, cloner);    //  Assign this from cloner
                return resolve(this);
            }
        });
    }


    


    sendText({recipientID, content, typingDelay = 0})   //  Show typing action, delay in second(s)
    {
        this.sendMessageObject({recipientID: recipientID, messageObj: {text: content}, typingDelay: typingDelay});
    }

    sendMessageObject({recipientID, messageObj, typingDelay = 0})
    {
        console.log('Begin send message for:', recipientID, messageObj);
        if (typingDelay > 0)
        {
            this.sendTypingAction({recipientID: recipientID});
        }


        setTimeout(() =>
        {
            request
            (
                {
                    method: 'POST',
                    url: process.env.MESSAGE_API,
                    qs: { access_token: this.#accessToken },
                    json:
                    {
                        recipient: {id: recipientID},
                        messaging_type: 'RESPONSE',
                        message: messageObj
                    }
                },
                (err, res, body) =>
                {
                    if (err || (res.statusCode !== 200))
                    {
                        console.log(`bot.js:120 - Can\'t send the message objects for: ${recipientID}' — Err:`, err || body);
                    }
                    else
                    {
                        console.log('Sended message objects for:', recipientID);
                    }
                }
            );
        }, typingDelay * 1000);
    }

    sendTypingAction({recipientID})
    {
        request
        (
            {
                method: 'POST',
                url: process.env.MESSAGE_API,
                qs: {access_token: this.#accessToken},
                headers: {'Content-Type': 'application/json'},
                json:
                {
                    recipient: {id: recipientID},
                    sender_action: "typing_on"
                }
            },
            (err, res, body) =>
            {
                if (err || (res.statusCode !== 200))
                {
                    console.log(`Can\'t send the Typing action for: ${recipientID}' — Err:`, err || body);
                }
                else
                {
                    console.log('Sended typing action for:', recipientID);
                }
            }
        );
    }



    //  Creating profile for ChatBot page
    createPersistentMenu({buttons, disabledComposer = false})
    {
        request
        (
            {
                method: 'POST',
                url: process.env.PROFILE_API,
                qs: {access_token: this.#accessToken},
                headers: {'Content-Type': 'application/json'},
                json:
                {
                    persistent_menu:
                    [
                        {
                            locale: "default",
                            composer_input_disabled: disabledComposer,
                            call_to_actions: buttons
                        }
                    ]
                }
            },
            (err, res, body) =>
            {
                if (err || (res.statusCode !== 200))
                {
                    console.log('Can\'t create the PersistentMenu buttons. Error:', err || body);
                }
                else
                {
                    console.log('Successfully create the PersistentMenu buttons. Result:', body);
                }
            }
        );
    }

    createGettingStarted()
    {
        request
        (
            {
                method: 'POST',
                url: process.env.PROFILE_API,
                qs: {access_token: this.#accessToken},
                headers: {'Content-Type': 'application/json'},
                json:
                {
                    get_started:
                    {
                        payload: "GET_STARTED"
                    }
                }
            },
            (err, res, body) =>
            {
                if (err || (res.statusCode !== 200))
                {
                    console.log('Can\'t create the Getstarted Payload. Error:', err || body);
                }
                else
                {
                    console.log('Successfully create the Getstarted Payload. Result:', body);
                }
            }
        );
    }

    createGreetingMessage({greetingContent})
    {
        /** Allowed to use following phrases in the greetingContent:
         * 
         * {{user_first_name}}
         * {{user_last_name}}
         * {{user_full_name}}
         * 
         */

        request
        (
            {
                method: 'POST',
                url: process.env.PROFILE_API,
                qs: {access_token: this.#accessToken},
                headers: {'Content-Type': 'application/json'},
                json:
                {
                    greeting:
                    [
                      {
                        locale: "default",
                        text: greetingContent
                      }
                    ]
                }
            },
            (err, res, body) =>
            {
                if (err || (res.statusCode !== 200))
                {
                    console.log('Can\'t create the GreetingMessage. Error:', err || body);
                }
                else
                {
                    console.log('Successfully create the GreetingMessage. Result:', body);
                }
            }
        );
    }

}




module.exports = Bot;