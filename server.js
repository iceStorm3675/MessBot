/** How to run test? Just type in terminal the following 2 commands:
 * 
 * npm run ngrok
 * npm start
 * 
 * 
 * 
 * Explain:
 *      npm run ngrok: will let the "ngrok" to forward your localhost's address to the internet.
 *                     Please make sure you installed ngrok in the Environment Variable.
 * 
 *      npm start:  will let the "nodemon" module to start the server and listen on port.
 *                  Every changes in your codes (after hit 'Ctrl + S' to save), 
 *                  nodemon will automatically re-run the server to apply the changes.
 */



require('dotenv/config');                       //  Use to access content in the ".env" file
const logger = require('morgan');               //  Use to log every request's infomation to this server.
const express = require('express');             //  The soul of this app - The server.
const bodyParser = require('body-parser');      //  Parsing the JSON, buffer, string and URL encoded data submitted using HTTP POST request.

const app = express();
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true})); //  extended: true  ==> allow to send the POST request with nested object.




//  Turn on the server and listen on port (by default is the port: 5000 - saved in the ".env" file).
const PORT = process.env.PORT;
app.listen(PORT, () =>
{
    console.log('ChatBot server listening on port:', PORT);
});




const webhookRoutes = require('./routes/webhook');
app.use('/webhook', webhookRoutes);     //  Add the '/webhook' middleware



//  Default route, to the home page.
app.get('/', (req, res) =>
{
    res.status(200).send('ChatBot server running Oke.');
});


//  This will be call by Cronjob schedules --- For keeping this app (after deployed on heroku) from sleeping.
app.get('/wakeupper', (req, res) =>
{
    res.status(200).send('Woke up !!');
});