
// self-define modules
const Bot = require('../bot/bot');
const Builder = require('../bot/message-builder');

//  node modules
require('dotenv/config');
const express = require('express');
const router = express.Router();



//  Handle when facebook send request to verify your server with your facebook app.
router.get('/', (req, res) =>
{
    if (req.query['hub.verify_token'] === process.env.VERIFY_TOKEN)
    {
        res.status(200).send(req.query['hub.challenge']);
    }
    else
    {
        res.status(404).send('Error, wrong validation token.');
    }
});



//  Handle when facebook user send message to the bot
router.post('/', async (req, res) =>
{
    try
    {
        //  First of all, check the req (webhook request) to determine whether this req is valid to initialize new bot
        {
            let messaging = req.body.entry[0].messaging[0];
            if (messaging.message)
            {
                if (messaging.message.app_id)    //  invalid ==> jump out to "finally" block
                    return;
            }
            else if (messaging.postback === undefined)    //  invalid ==> jump out to "finally" block
            {
                return;
            }
        }



        //  initialize the bot
        let bot = await new Bot(req);
        console.log('\nBOT:', bot);
        let msgBuilder = new Builder();



        //  clear the session if the postback is calling by facebook user
        if (bot.sender.Session)
        {
            if (bot.isPostBack)
            {
                bot.sender.changeSession("");
                bot.sender.Session = "";
            }
        }


        //  handle the session
        switch (bot.sender.Session)
        {
            case "EnterName":
            {
                bot.sender.changeSession("");
                bot.sendText(bot.sender.ID, `Your name is: ${bot.receivedText}.`, 1.25);
                break;
            }


            default:
            {
                if (bot.isPostBack)
                {
                    switch (bot.payload)
                    {
                        case "StartConversation":
                        {
                            bot.sendText(bot.sender.ID, "Enter your name ...", 1.25);
                            bot.sender.changeSession("EnterName");
                            break;
                        }
                    }
                }
                else if (bot.hasMessage)
                {
                    switch (bot.receivedText)
                    {
                        case "Hi":
                        {
                            bot.sendText({recipientID: bot.sender.ID, content: "Hi there !", typingDelay: 1.25});

                            // let postbackButton = msgBuilder.createButton({type: 'postback', title: 'Hello', payload: 'hello_btn'});
                            // let webUrlButton = msgBuilder.createButton({type: 'web_url', title: 'Browse', url: 'https://facebook.com'});
                            // let phoneButton = msgBuilder.createButton({type: 'phone_number', title: 'Call', payload: '+15105551234'});
                            // let buttons = [postbackButton, webUrlButton, phoneButton];


                            // let generic = msgBuilder.createGeneric(
                            // {
                            //     title: 'Facebook',
                            //     subtitle: 'Go to Facebook',
                            //     image_url: 'https://tinyurl.com/qk9ajzo',
                            //     default_action: msgBuilder.createDefaultAction({
                            //         url: 'https://facebook.com'
                            //     }),
                            //     buttons: msgBuilder.createButton({type: 'web_url', title: 'View', url: 'https://facebook.com'})
                            // });


                            // console.log(generic);
                            // let genericTemplate = msgBuilder.createGenericTemplate({elements: generic});
                            // // let buttonTemplate = msgBuilder.createButtonTemplate({title: 'Button template example', buttons: buttons});



                            let quickreply = msgBuilder.createQuickReply({
                                title: 'Yes',
                                content_type: 'text',
                                payload: 'USER_PHONE',
                                image_url: 'https://tinyurl.com/v92omjj'
                            });

                            let quickrepliesTemplate = msgBuilder.createQuickRepliesTemplate({title: 'Continue ?', quickReplies: quickreply});


                            // let postbackButton = msgBuilder.createButton({type: 'postback', title: 'Hello', payload: 'hello_btn'});
                            // let webUrlButton = msgBuilder.createButton({type: 'web_url', title: 'Browse', url: 'https://facebook.com'});
                            // let phoneButton = msgBuilder.createButton({type: 'phone_number', title: 'Call', payload: '+15105551234'});
                            // let buttonGroup = [postbackButton, webUrlButton, phoneButton];

                            // let buttonTemplate = msgBuilder.createButtonTemplate({title: 'Button template example', buttons: buttonGroup});
                            bot.sendMessageObject({recipientID: bot.sender.ID, messageObj: genericTemplate, typingDelay: 1.35});
                            break;
                        }
                        
                        default:
                        {
                            bot.sendText({recipientID: bot.sender.ID, content: "Sorry, I don't understand.", typingDelay: 1.25});
                        }
                    }
                }
            }
        }
    }
    catch (err)
    {
        console.log('\n[webhook.js] — Catched an error:', err);
    }
    finally
    {
        /**
         * Always return the 200 statusCode to Facebook's server.
         * - Otherwise, they will send to our server about 10 requests to check response.
         * - They may block our server from connecting to the facebook app if we don't response.
         */
 
        res.status(200).send('Oke.');
    }
});



module.exports = router;